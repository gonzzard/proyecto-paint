package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Gonzalo de las Heras
 */
public class Poligono extends Linea implements formas.Herramienta {

    /**
     * Objeto de tipo <code>ArrayList</code> que almacena las distintas líneas
     * del polígono.
     */
    ArrayList<Linea> lineas = new ArrayList();

    /**
     * Constructor de la clase.
     *
     * @param colorTrazo color del trazo del polígono.
     * @param anchoTrazo ancho del trazo del polígono.
     */
    public Poligono(Color colorTrazo, Stroke anchoTrazo) {
        super(colorTrazo, anchoTrazo);
    }

    /**
     * Método para finalizar una línea y puntarla.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(MouseEvent evt, Graphics2D g2) {
        super.finalizarTrazo(evt);
        super.pintar(g2);
    }

    /**
     * Método para pintar la última línea y las demás en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setStroke(anchoTrazo);
        this.finalizarTrazo();
        this.pintarLineas(g2);
    }

    /**
     * Método para definir la primera línea o la siguiente si ya hay.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    @Override
    public void iniciar(MouseEvent evt) {
        if (lineas.isEmpty()) {
            super.iniciar(evt);
        } else {
            this.IniciarSegmentoSiguiente();
        }
    }

    /**
     * Método para pintar las distintas líneas en un elemento gráfico.
     *
     * @param g2 elemento gráfico donde se pintará.
     */
    private void pintarLineas(Graphics2D g2) {
        Iterator iterador = lineas.iterator();
        while (iterador.hasNext()) {
            g2.draw((Shape) iterador.next());
        }
    }

    /**
     * Método para terminar una línea.
     */
    public void finalizarTrazo() {
        this.lineas.add(new Linea(x1, y1, x2, y2));
    }

    /**
     * Método para, una vez acabada una línea, inicar la siguiente desde el
     * punto de fin de la última.
     */
    public void IniciarSegmentoSiguiente() {
        this.x1 = x2;
        this.y1 = y2;
    }

    /**
     * Método para saber si se ha termiando de dibujar el polígono, cuando la
     * última línea corta a la primera.
     *
     * @return Delvuelve si se ha terminado de dibujar el polígono.
     */
    public boolean terminar() {
        if (lineas.size() >= 2 && this.intersectsLine(this.lineas.get(0))) {
            this.x2 = this.lineas.get(0).x1;
            this.y2 = this.lineas.get(0).y1;
            return true;
        }
        return false;
    }

    /**
     * Método para pintar la figura y saber si se ha terminado de dibujarla.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     * @return Devuelve si se va a dibujar un nuevo elemento.
     */
    @Override
    public boolean pintarTerminado(Graphics2D g2) {
        boolean resul;
        resul = this.terminar();
        this.pintar(g2);
        return resul;
    }
}
