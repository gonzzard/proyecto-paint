package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Gonzalo de las Heras
 * @version 1.0
 */
public class Elipse extends Ellipse2D.Double implements formas.Herramienta {

    /**
     * Variable de tipo <code>double</code> que indica la posición x de inicio
     * del rectángulo.
     */
    protected double xOrigen;
    /**
     * Variable de tipo <code>double</code> que indica la posición y de inicio
     * del rectángulo.
     */
    protected double yOrigen;
    /**
     * Variable de tipo <code>Color</code> que indica el color del trazo de la
     * figura.
     */
    Color colorTrazo;
    /**
     * Variable de tipo <code>Color</code> que indica el color del trazo de la
     * figura.
     */
    Color colorRelleno;
    /**
     * Variable de tipo <code>Stroke</code> que indica el color de relleno de la
     * figura.
     */
    Stroke anchoTrazo;

    /**
     * Constructor de la clase <code>Elipse</code>.
     *
     * @param colorTrazo Color del trazo.
     * @param anchoTrazo Ancho del trazo.
     * @param colorRelleno Color de relleno del cuadrado.
     */
    public Elipse(Color colorTrazo, Stroke anchoTrazo, Color colorRelleno) {
        super();
        this.colorTrazo = colorTrazo;
        this.anchoTrazo = anchoTrazo;
        this.colorRelleno = colorRelleno;
    }

    /**
     * Método para mover la posición actual de la elipse.
     */
    @Override
    public void mover() {

    }

    /**
     * Método para, una vez tenemos el inicio de la elipse, saber hasta donde se
     * extiende.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(java.awt.event.MouseEvent evt, Graphics2D g2) {
        if (evt.getX() > xOrigen) {
            this.width = evt.getX() - this.x;
        } else {
            this.x = evt.getX();
            this.width = xOrigen - this.x;
        }
        if (evt.getY() > yOrigen) {
            this.height = evt.getY() - yOrigen;
        } else {
            this.y = evt.getY();
            this.height = yOrigen - this.y;
        }
        this.pintar(g2);
    }

    /**
     * Método para pintar la elipse en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setStroke(this.anchoTrazo);
        g2.setColor(this.colorTrazo);
        g2.draw(this);
        g2.setPaint(colorRelleno);
        if (this.colorRelleno != null) {
            g2.fill(this);
        }
    }

    /**
     * Método para definir el punto de inicio de la elipse.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    @Override
    public void iniciar(java.awt.event.MouseEvent evt) {
        // Esquina superior izq circulo
        x = evt.getX();
        y = evt.getY();
        // Centro del circulo
        xOrigen = x;
        yOrigen = y;
    }

    /**
     * Método para pintar la figura y saber si se ha terminado de dibujarla.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     * @return Devuelve si se va a dibujar un nuevo elemento.
     */
    @Override
    public boolean pintarTerminado(Graphics2D g2) {
        this.pintar(g2);
        return true;
    }
}
