
package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;

/**
 *
 * @author Gonzalo de las Heras
 */
public class DibujoLibre extends Linea {

    /**
     * Lista de objetos de tipo <code>Linea</code> que almacena el conjunto de
     * todos los trazos.
     */
    ArrayList<Linea> trazos = new ArrayList<>();
    /**
     * Variable de tipo <code>int</code> que indica la posición del último trazo
     * dibujado.
     */
    int trazoActual = 0;
    /**
     * Objeto de tipo <code>Linea</code> empleada para unicializar el siguiente
     * trazo, una vez el anterior ha terminado.
     */
    Linea lineaAux;

    /**
     * Constructor de la clase <code>DibujoLibre</code>.
     * @param colorTrazo Color del trazo.
     * @param anchoTrazo Ancho del trazo.
     */
    public DibujoLibre(Color colorTrazo, Stroke anchoTrazo) {
        super(colorTrazo, anchoTrazo);
    }

    /**
     * Método para mover la posición actual de la figura.
     */
    @Override
    public void mover() {
    }

    /**
     * Método para, una vez tenemos el inicio de la figura, saber hasta donde se
     * extiende.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(java.awt.event.MouseEvent evt, Graphics2D g2) {
        g2.setStroke(anchoTrazo);
        g2.setColor(this.colorTrazo);
        // Defino el final del trazo actual.
        trazos.get(trazoActual).x2 = evt.getX();
        trazos.get(trazoActual).y2 = evt.getY();

        // Defino el siguiente trazo.
        if (trazos.get(trazoActual).x1 != trazos.get(trazoActual).x2
                || trazos.get(trazoActual).y1 != trazos.get(trazoActual).y2) {
            g2.drawLine((int) trazos.get(trazoActual).x1, (int) trazos.get(trazoActual).y1,
                    (int) trazos.get(trazoActual).x2, (int) trazos.get(trazoActual).y2);
            trazoActual++;
            lineaAux = new Linea(this.colorTrazo, this.anchoTrazo);
            lineaAux.x1 = trazos.get(trazoActual - 1).x2;
            lineaAux.y1 = trazos.get(trazoActual - 1).y2;
            lineaAux.x2 = trazos.get(trazoActual - 1).x2;
            lineaAux.y2 = trazos.get(trazoActual - 1).y2;
            trazos.add(lineaAux);
        }
    }

    /**
     * Método para pintar la figura en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setColor(this.colorTrazo);
        g2.setStroke(anchoTrazo);
        for (Linea trazo : trazos) {
            g2.drawLine((int) trazo.x1, (int) trazo.y1, (int) trazo.x2, (int) trazo.y2);
        }
    }

    /**
     * Método para definir el punto de inicio de la figura.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    @Override
    public void iniciar(java.awt.event.MouseEvent evt) {
        lineaAux = new Linea(this.colorTrazo, this.anchoTrazo);
        lineaAux.x1 = evt.getX();
        lineaAux.y1 = evt.getY();
        lineaAux.x2 = evt.getX();
        lineaAux.y2 = evt.getY();   
        trazos.add(lineaAux);
    }

}
