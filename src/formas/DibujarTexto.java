package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

/**
 *
 * @author Gonzalo de las Heras
 */
public class DibujarTexto extends Linea implements Herramienta {

    /**
     * Variable de tipo <code>String</code> que guarda el texto escrito.
     */
    String texto;
    /**
     * Variable de tipo <code>int</code> que indica la posición x del cuadro de
     * texto.
     */
    int posX;
    /**
     * Variable de tipo <code>int</code> que indica la posición y del cuadro de
     * texto.
     */
    int posY;

    /**
     * Constructor de la clase DibujarTexto.
     *
     * @param texto Texto escrito.
     * @param color Color del texto.
     * @param ancho Tamaño de la imagen.
     */
    public DibujarTexto(String texto, Color color, Stroke ancho) {
        super(color, ancho);
        this.texto = texto;
    }

    /**
     * Método para mover la posición actual de la figura.
     */
    @Override
    public void mover() {

    }

    /**
     * Método para, una vez tenemos el inicio de la figura, saber hasta donde se
     * extiende.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(java.awt.event.MouseEvent evt, Graphics2D g2) {
        g2.setColor(this.colorTrazo);
        g2.drawString(texto, evt.getX(), evt.getY());
        posX = evt.getX();
        posY = evt.getY();
    }

    /**
     * Método para pintar la figura en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setColor(this.colorTrazo);
        g2.drawString(texto, posX, posY);
    }

    /**
     * Método para definir el punto de inicio de la figura.
     *
     * @param texto Evento empleado para saber la posición actual del ratón.
     */
    public void iniciar(String texto) {
        this.texto = texto;

    }

}
