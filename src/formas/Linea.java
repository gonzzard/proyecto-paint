package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;

/**
 *
 * @author Gonzalo de las heras
 */
public class Linea extends Line2D.Double implements formas.Herramienta {

    /**
     * Objeto de tipo <code>Color</code> que almacena el color del trazo de la
     * Línea.
     */
    Color colorTrazo;
    /**
     * Variable de tipo <code>double</code> que indica la posición x de inicio
     * del rectángulo.
     */
    Stroke anchoTrazo;

    /**
     * Constructor por defecto.
     *
     * @param colorTrazo color del trazo del polígono.
     * @param anchoTrazo ancho del trazo del polígono.
     */
    public Linea(Color colorTrazo, Stroke anchoTrazo) {
        super();
        this.colorTrazo = colorTrazo;
        this.anchoTrazo = anchoTrazo;
    }

    /**
     * Constructor especificando su punto de inicio y de fin.
     *
     * @param x1 coordenada x del punto de inicio.
     * @param y1 coordenada y del punto de inicio.
     * @param x2 coordenada x del punto de fin.
     * @param y2 coordenada y del punto de fin.
     */
    public Linea(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    /**
     * Método para mover la posición actual de la línea.
     */
    @Override
    public void mover() {

    }

    /**
     * Método para, una vez tenemos el inicio de la línea, saber hasta donde se
     * extiende.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(MouseEvent evt, Graphics2D g2) {
        this.finalizarTrazo(evt);
        this.pintar(g2);
    }

    /**
     * Método para pintar la línea en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setStroke(anchoTrazo);
        g2.setColor(colorTrazo);
        g2.draw(this);
    }

    /**
     * Método para definir la línea.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    @Override
    public void iniciar(MouseEvent evt) {
        this.iniciarTrazo(evt);
    }

    /**
     * Método para definir el punto de inicio de la línea.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    public void iniciarTrazo(MouseEvent evt) {
        // Punto inicio
        this.x1 = evt.getX();
        this.y1 = evt.getY();
        this.x2 = evt.getX();
        this.y2 = evt.getY();
    }

    /**
     * Método para definir el punto de fin de la línea.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    public void finalizarTrazo(MouseEvent evt) {
        // Punto fin
        this.x2 = evt.getX();
        this.y2 = evt.getY();
    }

    /**
     * Método para pintar la figura y saber si se ha terminado de dibujarla.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     * @return Devuelve si se va a dibujar un nuevo elemento.
     */
    @Override
    public boolean pintarTerminado(Graphics2D g2) {
        this.pintar(g2);
        return true;
    }

}
