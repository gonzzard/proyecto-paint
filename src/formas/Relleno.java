/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Deque;
import java.util.LinkedList;

/**
 *
 * @author Gonzalo de las Heras
 */
public class Relleno extends Poligono implements formas.Herramienta {

    /**
     * Objeto de tipo <code>img</code> empleado la imagen rellenada.
     */
    BufferedImage img;

    /**
     * Constructor de la clase
     *
     * @param colorRellenoSelec Color de relleno seleccionado.
     * @param bufferPanel Buffer que contiene la sección a rellenar.
     * @param evt Evento del ratón (empleado para saber el punto donde se hizo
     * click).
     */
    public Relleno(Color colorRellenoSelec, BufferedImage bufferPanel, java.awt.event.MouseEvent evt) {
        super(null, null);
        this.rellenar(bufferPanel, new Point(evt.getX(), evt.getY()), new Color(bufferPanel.getRGB(evt.getX(),
                evt.getY())), colorRellenoSelec);
        this.img = copiarBuffer(bufferPanel);
    }

    /**
     * Método para pintar la figura en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.drawImage(img, null, 0, 0);
    }

    /**
     * Método para rellenar una sección de un color.
     *
     * @param image Imagen que contiene la sección a rellenar.
     * @param node Primer punto desde donde buscar el resto de puntos a pintar.
     * @param targetColor Color el cual se quiere sustituir.
     * @param replacementColor Color con el que se quiere rellenar la sección.
     * @return Devuelve la lista enlazada de puntos a pintar.
     */
    public Deque rellenar(BufferedImage image, Point node, Color targetColor, Color replacementColor) {
        /* Variables */
        int width = image.getWidth();
        int height = image.getHeight();
        int target = targetColor.getRGB();
        int replacement = replacementColor.getRGB();
        Deque<Point> queue1 = new LinkedList<>();
        if (target != replacement) {
            Deque<Point> queue = new LinkedList<Point>();

            do {
                int x = node.x;
                int y = node.y;
                while (x > 0 && image.getRGB(x - 1, y) == target) {
                    x--;
                }
                boolean spanUp = false;
                boolean spanDown = false;
                while (x < width && image.getRGB(x, y) == target) {
                    image.setRGB(x, y, replacement);
                    if (!spanUp && y > 0 && image.getRGB(x, y - 1) == target) {
                        queue.add(new Point(x, y - 1));
                        queue1.add(new Point(x, y - 1));
                        spanUp = true;
                    } else if (spanUp && y > 0 && image.getRGB(x, y - 1) != target) {
                        spanUp = false;
                    }
                    if (!spanDown && y < height - 1 && image.getRGB(x, y + 1) == target) {
                        queue.add(new Point(x, y + 1));
                        queue1.add(new Point(x, y + 1));
                        spanDown = true;
                    } else if (spanDown && y < height - 1 && image.getRGB(x, y + 1) != target) {
                        spanDown = false;
                    }
                    x++;
                }
            } while ((node = queue.pollFirst()) != null);
            return queue1;
        }
        return queue1;

    }

    private BufferedImage copiarBuffer(BufferedImage buffer) {
        ColorModel cm = buffer.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = buffer.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

}
