package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Gonzalo de las heras
 */
public class Rectangulo extends Rectangle2D.Double implements formas.Herramienta {

    /**
     * Variable que indica la posición x de inicio del rectángulo.
     */
    protected double xOrigen;
    /**
     * Variable que indica la posición y de inicio del rectángulo.
     */
    protected double yOrigen;
    /**
     * Variable que indica el color del trazo de la figura.
     */
    Color colorTrazo;
    /**
     * Variable que indica el color del trazo de la figura.
     */
    Color colorRelleno;
    /**
     * Variable que indica el color de relleno de la figura.
     */
    Stroke anchoTrazo;

    /**
     * Constructor por defecto.
     *
     * @param colorTrazo color del trazo.
     * @param anchoTrazo ancho del trazo.
     * @param colorRelleno color de relleno.
     */
    public Rectangulo(Color colorTrazo, Stroke anchoTrazo, Color colorRelleno) {
        super();
        this.colorTrazo = colorTrazo;
        this.anchoTrazo = anchoTrazo;
        this.colorRelleno = colorRelleno;
    }

    /**
     * Método para mover la posición actual del rectángulo.
     */
    @Override
    public void mover() {
    }

    /**
     * Método para, una vez tenemos el inicio del rectángulo, saber hasta donde
     * se extiende.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(java.awt.event.MouseEvent evt, Graphics2D g2) {
        if (evt.getX() > xOrigen) {
            this.width = evt.getX() - this.x;
        } else {
            this.x = evt.getX();
            this.width = xOrigen - this.x;
        }
        if (evt.getY() > yOrigen) {
            this.height = evt.getY() - yOrigen;
        } else {
            this.y = evt.getY();
            this.height = yOrigen - this.y;
        }
        this.pintar(g2);
    }

    /**
     * Método para pintar el rectángulo en un elemento gráfico.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     */
    @Override
    public void pintar(Graphics2D g2) {
        g2.setStroke(this.anchoTrazo);
        g2.setColor(this.colorTrazo);
        g2.draw(this);
        g2.setPaint(colorRelleno);
        if (this.colorRelleno != null) {
            g2.fill(this);
        }
    }

    /**
     * Método para definir el punto de inicio del rectángulo.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    @Override
    public void iniciar(java.awt.event.MouseEvent evt) {
        // Esquina superior izq rectangulo
        xOrigen = evt.getX();
        yOrigen = evt.getY();
        this.x = xOrigen;
        this.y = yOrigen;
    }

    /**
     * Método para pintar la figura y saber si se ha terminado de dibujarla.
     *
     * @param g2 Elemento gráfico para saber donde pintar.
     * @return Devuelve si se va a dibujar un nuevo elemento.
     */
    @Override
    public boolean pintarTerminado(Graphics2D g2) {
        this.pintar(g2);
        return true;
    }
}
