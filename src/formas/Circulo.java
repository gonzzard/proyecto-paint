package formas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 *
 * @author Gonzalo de las heras
 */
public class Circulo extends Elipse {

    public double radio;

    /**
     * Constructor por defecto.
     * @param colorTrazo color del trazo.
     * @param anchoTrazo ancho del trazo.
     * @param colorRelleno color de relleno.
     */
    public Circulo(Color colorTrazo, Stroke anchoTrazo, Color colorRelleno) {
        super(colorTrazo, anchoTrazo, colorRelleno);
    }

    /**
     * Método para mover la posición actual del círculo.
     */
    @Override
    public void mover() {
    }

    /**
     * Método para, una vez tenemos el centro del círculo, extenderlo desde el
     * mismo hasta donde indique el ratón.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     * @param g2 Elemento gráfico para saber donde pintar la elipse, una vez
     * tenemos sus paremétros definidos.
     */
    @Override
    public void reposicionar(java.awt.event.MouseEvent evt, Graphics2D g2) {
        this.calcularRadio(evt);
        this.x = xOrigen - radio;
        this.y = yOrigen - radio;
        this.height = radio * 2;
        this.width = this.height;
        this.pintar(g2);
    }

    /**
     * Método para calcular el centro del círculo.
     *
     * @param evt Evento empleado para saber la posición actual del ratón.
     */
    public void calcularRadio(java.awt.event.MouseEvent evt) {
        double d1, d2;
        d1 = evt.getX() - xOrigen;
        if (d1 < 0) {
            d1 = xOrigen - evt.getX();
        }
        d2 = evt.getY() - yOrigen;
        if (d2 < 0) {
            d2 = yOrigen - evt.getY();
        }
        radio = pow(d1, 2) + pow(d2, 2); // modulo vector
        if (radio < 0) {
            radio = radio * -1;
        }
        radio = sqrt(radio);
        
        
    }

}
