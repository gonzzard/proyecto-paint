
import formas.Herramienta;
import java.awt.Color;
import javax.swing.JPanel;
import utilidades.*;

/**
 *
 * @author Gonzalo de las Heras
 */
public class Paint {

    /**
     * Objeto de tipo <code>Doblebuffer</code> que contiene los elementos
     * gráficos del programa.
     */
    private final DobleBuffer dobleBuffer;
    /**
     * Objeto de tipo <code>GestorArchivos</code> que contiene las
     * funcionalidades de gestión de archivos.
     */
    private final GestorArchivos gestorArchivos;
    /**
     * Objeto de tipo <code>GestorColores</code> que contiene los colores
     * empleados a la hora de dibujar los distintos elementos.
     */
    private final GestorColores gestorColores;
    /**
     * Objeto de tipo <code>GestorTrazos</code> que contiene los distintos
     * trazos empleados a la hora de dibujar los distintos elementos.
     */
    private final GestorTrazos gestorTrazos;
    /**
     * Objeto de tipo <code>FabricaFormas</code> que se encarga de crear las
     * distintas formas disponibles.
     */
    private final FabricaFormas fabricaFormas;
    /**
     * Objeto de tipo <code>ObjetosPintados</code> que contiene los distintos
     * elementos dibujados.
     */
    private final ObjetosPintados elementosDibujados;
    /**
     * Objeto de tipo <code>Cursores</code> que contiene los disntintos cursores
     * empleados en la aplicación.
     */
    private final Cursores cursores;
    /**
     * Variable de tipo <code>int</code> que almacena el valor de la opción de
     * dibujado que está seleccionada
     *
     * -Opcion 1: Circulo<br> Opcion 2: Cuadrado<br> Opcion 3: Linea<br> Opcion
     * 4: Libre<br> Opcion 5: Elipse<br> Opcion 6: Rectangulo.
     */
    private int opcion;
    /**
     * Variable de tipo <code>int</code> que almacena el valor del cursor
     * correspondiente a cada opción.
     */
    private int selecCursor;
    /**
     * Variable de tipo <code>boolean</code> que indica si se va a dibujar un
     * nuevo dibujo, empleado para cuando se dubuja un polígono.
     */
    private boolean nuevoDibujo;
    /**
     * Objeto de tipo <code>Herramienta</code> que almacena la herramienta de
     * dibujo que se emplea en ese momento.
     */
    private Herramienta herramientaActual;

    /**
     * Método que devuelve el objeto <code>Doblebuffer</code>.
     *
     * @return Devuelve el objeto <code>Doblebuffer</code>.
     */
    public DobleBuffer getDobleBuffer() {
        return dobleBuffer;
    }

    /**
     * Método que devuelve el objeto <code>GestorArchivos</code>.
     *
     * @return Devuelve el objeto <code>GestorArchivos</code>.
     */
    public GestorArchivos getGestorArchivos() {
        return gestorArchivos;
    }

    /**
     * Método que devuelve el objeto <code>GestorColores</code>.
     *
     * @return Devuelve el objeto <code>GestorColores</code>.
     */
    public GestorColores getGestorColores() {
        return gestorColores;
    }

    /**
     * Método que devuelve el objeto <code>GestorTrazos</code>.
     *
     * @return Devuelve el objeto <code>GestorTrazos</code>.
     */
    public GestorTrazos getGestorTrazos() {
        return gestorTrazos;
    }

    /**
     * Método que devuelve el objeto <code>FabricaFormas</code>.
     *
     * @return Devuelve el objeto <code>FabricaFormas</code>.
     */
    public FabricaFormas getFabricaFormas() {
        return fabricaFormas;
    }

    /**
     * Método que devuelve el objeto <code>ObjetosDibujados</code>.
     *
     * @return Devuelve el objeto <code>ObjetosDibujados</code>.
     */
    public ObjetosPintados getElementosDibujados() {
        return elementosDibujados;
    }

    /**
     * Método que devuelve el objeto <code>Herramienta</code>.
     *
     * @return Devuelve el objeto <code>Herramienta</code>.
     */
    public Herramienta getHerramientaActual() {
        return herramientaActual;
    }

    /**
     * Método que devuelve el objeto <code>Cursores</code>.
     *
     * @return Devuelve el objeto <code>Cursores</code>.
     */
    public Cursores getCursores() {
        return cursores;
    }

    /**
     * Método que actualiza el valor de herramientaActual.
     *
     * @param herramientaActual Actualiza el valor de herramientaActual.
     */
    public void setHerramientaActual(Herramienta herramientaActual) {
        this.herramientaActual = herramientaActual;
    }

    /**
     * Método que devuelve el cursor seleccionado.
     *
     * @return Devuelve el cursor seleccionado.
     */
    public int getSelecCursor() {
        return selecCursor;
    }

    /**
     * Método que actualiza el valor del cursor seleccionado.
     *
     * @param selecCursor Actualiza el valor del cursor seleccionado.
     */
    public void setSelecCursor(int selecCursor) {
        this.selecCursor = selecCursor;
    }

    /**
     * Método que devuelve el valor de la opción actual.
     *
     * @return Devuelve el valor de la opción actual.
     */
    public int getOpcion() {
        return opcion;
    }

    /**
     * Método que actualiza el valor de la opción actual.
     *
     * @param opcion Actualiza el valor de la opción actual.
     */
    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    /**
     * Método que devuelve si se va a dibujar un nuevo elemento.
     *
     * @return Devuelve si se va a dibujar un nuevo elemento.
     */
    public boolean isNuevoDibujo() {
        return nuevoDibujo;
    }

    /**
     * Método que actualiza si se va a dibujar un nuevo elemento.
     *
     * @param nuevoDibujo Actualiza si se va a dibujar un nuevo elemento.
     */
    public void setNuevoDibujo(boolean nuevoDibujo) {
        this.nuevoDibujo = nuevoDibujo;
    }

    /**
     * Constructor de la clase <code>Paint</code>
     *
     * @param panelLienzo <code>JPanel</code> para asociar con el
     * <code>DobleBuffer</code>.
     * @param selectorFicheros <code>JFileChooser</code> para asociar con el
     * <code>GestorArchivos</code>.
     * @param anchoTrazo <code>JSlider</code> para asociar con
     * el<code>GestorTrazos</code>.
     */
    public Paint(JPanel panelLienzo, javax.swing.JFileChooser selectorFicheros, javax.swing.JSlider anchoTrazo) {
        this.dobleBuffer = new DobleBuffer(panelLienzo, panelLienzo.getWidth(), panelLienzo.getHeight());
        this.gestorArchivos = new GestorArchivos(selectorFicheros);
        this.gestorColores = new GestorColores();
        this.gestorTrazos = new GestorTrazos(anchoTrazo);
        this.fabricaFormas = new FabricaFormas();
        this.elementosDibujados = new ObjetosPintados();
        this.herramientaActual = null;
        this.nuevoDibujo = true;
        this.opcion = 4;
        this.selecCursor = 0;
        this.cursores = new Cursores();
    }

    /**
     * Método que inicializa el objeto que se esta dibujando en ese momento.
     *
     * @param evt Posición actual del ratón.
     * @param texto Texto escrito a dibujar (si hay).
     */
    public void eventoPintarPressed(java.awt.event.MouseEvent evt, String texto) {
        this.setHerramientaActual(this.getFabricaFormas().creaForma(this.getOpcion(), this.getGestorColores().getColorTrazoSelec(), this.getGestorColores().getColorRellenoSelec(),
                this.getGestorTrazos().getAnchoTrazoSelec(), texto, evt, this.getDobleBuffer(), this.getElementosDibujados()));
        this.getHerramientaActual().iniciar(evt);
    }

    /**
     * Método para ir reposicionando la figura que se está dibujando en ese
     * momento.
     *
     * @param evt Posición posDibujoActual del ratón.
     */
    public void eventoPintarDragged(java.awt.event.MouseEvent evt) {
        this.getDobleBuffer().getGraficaPanel().drawImage(this.getDobleBuffer().getBufferPanel(), 0, 0, null);
        if (this.getOpcion() == 4 || this.getOpcion() == 8) {
            this.getHerramientaActual().reposicionar(evt, this.getDobleBuffer().getGraficaBuffer());
        } else if (this.getOpcion() != 7 && this.getOpcion() != 9) {
            this.getHerramientaActual().reposicionar(evt, this.getDobleBuffer().getGraficaPanel());
        }
    }

    /**
     * Método que controla el evento de soltar el ratón.
     *
     */
    public void eventoPintarReleased() {
        if (this.getOpcion() != 9) {
            // Se añade al dibujo a la lista de dibujos
            if (this.getFabricaFormas().nuevoDibujo) {
                this.getElementosDibujados().aniadir(this.getHerramientaActual());
            }
            this.getFabricaFormas().nuevoDibujo = this.getElementosDibujados().pintarTodosElementos(this.getDobleBuffer().getGraficaBuffer());
            // Vuelco en el lienzo
            this.getDobleBuffer().getGraficaPanel().drawImage(this.getDobleBuffer().getBufferPanel(), 0, 0, null);
        }
    }

    /**
     * Método para guardar el lienzo en un archivo de imagen.
     *
     * @param selectorFicheros <code>JFileChooser</code> empleado para navegar
     * entre los archivos del equipo.
     *
     */
    public void guardarArchivo(javax.swing.JFileChooser selectorFicheros) {
        this.gestorArchivos.guardarArchivo(this.dobleBuffer.getBufferPanel(), this.gestorArchivos.getSelectorFicheros());
    }

    /**
     * Método para abrir un archivo de imagen y llevarlo al lienzo.
     *
     * @param selectorFicheros <code>JFileChooser</code> empleado para navegar
     * entre los archivos del equipo.
     */
    public void abrirArchivo(javax.swing.JFileChooser selectorFicheros) {
        this.gestorArchivos.abrirArchivo(this.dobleBuffer.getGraficaBuffer(), selectorFicheros);
    }

    /**
     * Método para limpar el lienzo y elimina todos los dibujos que haya.
     *
     * @param panelLienzo JPanel a limpiar.
     */
    public void limpiar(JPanel panelLienzo) {
        this.elementosDibujados.vaciar();
        this.dobleBuffer.getGraficaBuffer().setColor(Color.white);
        this.dobleBuffer.getGraficaBuffer().fillRect(0, 0, panelLienzo.getWidth(), panelLienzo.getHeight());
        this.dobleBuffer.getGraficaPanel().setColor(Color.white);
        this.dobleBuffer.getGraficaPanel().fillRect(0, 0, panelLienzo.getWidth(), panelLienzo.getHeight());

    }
}
