package utilidades;

import formas.DibujoLibre;
import formas.Herramienta;
import formas.Relleno;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Gonzalo de las heras
 */
public class ObjetosPintados {

    /**
     * Objeto de tipo <code>ArrayList</code> donde se guardan las figuras
     * creadas.
     */
    public final ArrayList<Herramienta> elementosDibujados;
    /**
     * Variable de tipo <code>int</code> que apunta a una posición de la array
     * de objetos.
     */
    public int posDibujoActual;

    /**
     * Constructor de la clase.
     */
    public ObjetosPintados() {
        this.elementosDibujados = new ArrayList<>();
        this.posDibujoActual = 0;
    }

    /**
     * Método que borra las figuras de la lista desde donde indique
     * <code>posDibujoActual</code>.
     */
    public void borrarElemDeshechos() {
        for (int i = this.elementosDibujados.size() - 1; i >= this.posDibujoActual; i--) {
            this.elementosDibujados.remove(i);
        }
    }

    /**
     * Método para añadir una figura a la lista de figuras.
     *
     * @param herramientaActual Elemento a añadir.
     */
    public void aniadir(Herramienta herramientaActual) {
        this.elementosDibujados.add(herramientaActual);
        this.posDibujoActual++;
    }

    /**
     * Método que pinta todos los elementos del <code>ArrayList</code> de
     * elementos dibujados.
     *
     * @param grafica Gráfica donde se pintarán todos los elementos.
     * @return Devuelve si se ha terminadode pintar el dibujo, esto es para
     * cuando hay un polígono no me considere cada lado como un objeto
     * <code>Polígono</code> distinto.
     */
    public boolean pintarTodosElementos(Graphics2D grafica) {
        // for (int i = 0; i < elementosDibujados.size(); i++)
        boolean nuevoDibujo = false;
        for (Herramienta elementosDibujado : elementosDibujados) {
            if (elementosDibujado instanceof Relleno) {
                elementosDibujado.pintar(grafica);
                nuevoDibujo = true;
            } else {
                nuevoDibujo = elementosDibujado.pintarTerminado(grafica);
            }
        }
        return nuevoDibujo;
    }

    /**
     * Método que pinta todos los elementos desde la posición posDibujoActual
     * (posDibujoActual indica la posición del <code> ArrayList</code>, esto es
     * útil para saber en que lugar de esta estructura estamos cuando el usuario
     * pulsa el botón de deshacer o de rehacer) menos uno, en el
     * <code>jPanel</code>.
     *
     * @param graficaBuffer Gráfica del buffer donde dibujará.
     * @param graficaPanel Gráfica del panel donde se volcará la imagen.
     * @param bufferPanel Buffer del panel donde se volcará la imagen.
     * @return Devuelve si se han deshecho (borrado) todos las figuras que había
     * (esto sirve para saber si se tiene que habilitar los botones de deshacer
     * y rehacer).
     */
    public boolean pintarElemenosMenosUno(Graphics2D graficaBuffer, Graphics2D graficaPanel, BufferedImage bufferPanel) {
        int elemActual = posDibujoActual - 1;
        for (int i = 0; i < elemActual; i++) {
            if (elementosDibujados.get(i) instanceof DibujoLibre) {
                elementosDibujados.get(i).pintar(graficaBuffer);
            } else if (elementosDibujados.get(i) instanceof Relleno) {
                elementosDibujados.get(i).pintar(graficaBuffer);
            } else {
                elementosDibujados.get(i).pintar(graficaBuffer);
            }
        }
        graficaPanel.drawImage(bufferPanel, 0, 0, null);
        posDibujoActual--;
        if (elemActual == 0) {
            return false;
        }
        return true;
    }

    /**
     * Método que pinta todos los elementos desde la posición posDibujoActual
     * (posDibujoActual indica la posición del <code> ArrayList</code>, esto es
     * útil para saber en que lugar de esta estructura estamos cuando el usuario
     * pulsa el botón de deshacer o de rehacer) más uno, en el
     * <code>jPanel</code>.
     *
     * @param graficaBuffer Gráfica del buffer donde dibujará.
     * @param graficaPanel Gráfica del panel donde se volcará la imagen.
     * @param bufferPanel Buffer del panel donde se volcará la imagen.
     * @return Devuelve si se han pintado todos las figuras que había (esto
     * sirve para saber si se tiene que habilitar los botones de deshacer y
     * rehacer).
     */
    public boolean pintarElemenosMasUno(Graphics2D graficaBuffer, Graphics2D graficaPanel, BufferedImage bufferPanel) {
        int elemActual = posDibujoActual + 1;
        for (int i = 0; i < elemActual; i++) {
            if (elementosDibujados.get(i) instanceof DibujoLibre) {
                elementosDibujados.get(i).pintar(graficaBuffer);
            } else if (elementosDibujados.get(i) instanceof Relleno) {
                elementosDibujados.get(i).pintar(graficaBuffer);
            } else {
                elementosDibujados.get(i).pintar(graficaBuffer);
            }
        }
        graficaPanel.drawImage(bufferPanel, 0, 0, null);
        posDibujoActual++;
        if (elemActual == elementosDibujados.size()) {
            return false;
        }
        return true;
    }

    /**
     * Método para limpar el lienzo y elimina todos los dibujos que haya.
     */
    public void vaciar() {
        this.elementosDibujados.clear();
        this.posDibujoActual = 0;
    }
}
