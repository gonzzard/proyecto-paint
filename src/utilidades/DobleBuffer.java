package utilidades;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Gonzalo de las heras.
 */
public class DobleBuffer {

    /**
     * Objeto de tipo <code>BufferImage</code> que almacena el buffer del panel
     * y un buffer del tamaño del panel y de color blanco.
     */
    private final BufferedImage bufferPanel;
    /**
     * Objeto de tipo <code>BufferImage</code> que almacena un buffer vacío.
     */
    private final BufferedImage bufferVacio;
    /**
     * Objeto de tipo <code>Graphics2D</code> que almacena los elementos
     * gráficos del buffer.
     */
    private final Graphics2D graficaBuffer;
    /**
     * Objeto de tipo <code>JPanel</code> que indica el lienzo donde se va a
     * dibujar.
     */
    JPanel panelLienzo;

    /**
     * Objeto de tipo <code>Graphics2D</code> que almacena los elementos
     * gráficos del panel.
     */
    private final Graphics2D graficaPanel;

    /**
     * Constructor de la clase.
     *
     * @param panelLienzo objeto de tipo <code>JPanel</code> que indica el
     * lienzo donde se va a dibujar.
     * @param ancho ancho de lienzo.
     * @param alto alto del lienzo.
     */
    public DobleBuffer(JPanel panelLienzo, int ancho, int alto) {
        this.panelLienzo = panelLienzo;
        this.bufferPanel = (BufferedImage) this.panelLienzo.createImage(ancho, alto);
        this.bufferVacio = copiarBuffer(bufferPanel);
        Graphics2D g3 = bufferPanel.createGraphics();
        g3.setColor(Color.white);
        g3.fillRect(0, 0, ancho, alto);
        this.graficaPanel = (Graphics2D) panelLienzo.getGraphics();
        this.graficaBuffer = (Graphics2D) bufferPanel.getGraphics();
    }

    /**
     * Método para copiar un Buffer
     *
     * @param buffer buffer a copiar.
     * @return Devuelve un objeto BufferedImage que es una copia del buffer
     * entregado
     */
    private BufferedImage copiarBuffer(BufferedImage buffer) {
        ColorModel cm = buffer.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = buffer.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    /**
     * Método para deshacer un cambio.
     *
     * @param elementosDibujados Dibujos hechos.
     * @param botonRehacer Botón de rehacer.
     * @param botonDeshacer Botón de deshacer.
     * @param colorTrazoSelec Trazo seleccionado.
     */
    public void deshacer(ObjetosPintados elementosDibujados, JButton botonRehacer, JButton botonDeshacer, Color colorTrazoSelec) {
        botonRehacer.setEnabled(true);
        this.getGraficaBuffer().setColor(Color.white);
        this.getGraficaBuffer().fillRect(0, 0, this.panelLienzo.getWidth(), panelLienzo.getHeight());
        this.getGraficaBuffer().setColor(colorTrazoSelec);
        botonDeshacer.setEnabled(elementosDibujados.pintarElemenosMenosUno(this.getGraficaBuffer(), this.getGraficaPanel(), this.getBufferPanel()));
    }

    /**
     * Método para rehacer un cambio previamente deshecho con la opción de
     * deshacer.
     *
     * @param elementosDibujados Dibujos hechos.
     * @param botonRehacer Botón de rehacer.
     * @param botonDeshacer Botón de deshacer.
     * @param colorTrazoSelec Trazo seleccionado.
     */
    public void rehacer(ObjetosPintados elementosDibujados, JButton botonRehacer, JButton botonDeshacer, Color colorTrazoSelec) {
        botonDeshacer.setEnabled(true);
        this.getGraficaBuffer().setColor(Color.white);
        this.getGraficaBuffer().fillRect(0, 0, panelLienzo.getWidth(), panelLienzo.getHeight());
        this.getGraficaBuffer().setColor(colorTrazoSelec);
        botonRehacer.setEnabled(elementosDibujados.pintarElemenosMasUno(this.getGraficaBuffer(), this.getGraficaPanel(), this.getBufferPanel()));
    }
    /**
     * Método que devuelve la gráfica del buffer.
     *
     * @return Devuelve la gráfica del buffer.
     */
    public Graphics2D getGraficaBuffer() {
        return graficaBuffer;
    }
    /**
     * Método que devuelve la gráfica del panel.
     *
     * @return Devuelve la gráfica del panel.
     */
    public Graphics2D getGraficaPanel() {
        return graficaPanel;
    }
    /**
     * Método que devuelve el buffer del panel.
     *
     * @return Devuelve el buffer del panel.
     */
    public BufferedImage getBufferPanel() {
        return bufferPanel;
    }
    /**
     * Método que devuelve el buffer vacío.
     *
     * @return Devuelve el buffer vacío.
     */
    public BufferedImage getBufferVacio() {
        return bufferVacio;
    }

}
