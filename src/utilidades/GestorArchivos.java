
package utilidades;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Gonzalo de las heras
 */
public class GestorArchivos {

    /**
     * Objeto de tipo <code>JFileChooser</code> usado para navegar entre los
     * archivos de la máquina.
     */
    private final javax.swing.JFileChooser selectorFicheros;

    /**
     * Método que devuelve el <code>JFileChooser</code> usado para navegar entre
     * los archivos de la máquina.
     *
     * @return Devuelve el <code>JFileChooser</code> usado para navegar entre
     * los archivos de la máquina.
     */
    public JFileChooser getSelectorFicheros() {
        return selectorFicheros;
    }

    /**
     * Constructor de la clase.
     *
     * @param selectorFicheros <code>JFileChooser</code> usado para navegar
     * entre los archivos de la máquina.
     */
    public GestorArchivos(javax.swing.JFileChooser selectorFicheros) {
        this.selectorFicheros = selectorFicheros;
    }

    /**
     * Método para abrir un archivo de imagen y llevarlo al lienzo.
     *
     * @param graficaBuffer Gráfica donde se volcará el archivo de imagen.
     * @param selectorFicheros <code>JFileChooser</code> usado para navegar
     * entre los archivos de la máquina.
     */
    public void abrirArchivo(Graphics2D graficaBuffer, javax.swing.JFileChooser selectorFicheros) {
        int seleccion = selectorFicheros.showOpenDialog(selectorFicheros);
        if (seleccion == JFileChooser.APPROVE_OPTION) {
            File fichero = selectorFicheros.getSelectedFile();
            try {
                BufferedImage imagen = ImageIO.read(fichero);
                graficaBuffer.drawImage(imagen, 0, 0, null);
            } catch (IOException ex) {
            }
        }
    }

    /**
     * Método para guardar lo dibujado en el <code>jPanel</code> enun archivo de
     * imagen.
     *
     * @param bufferPanel Buffer de donde se sacará la imagen que se va a
     * guardar.
     * @param selectorFicheros <code>JFileChooser</code> usado para navegar
     * entre los archivos de la máquina.
     */
    public void guardarArchivo(BufferedImage bufferPanel, javax.swing.JFileChooser selectorFicheros) {
        int seleccion = selectorFicheros.showSaveDialog(selectorFicheros);
        selectorFicheros.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp"));
        if (seleccion == JFileChooser.APPROVE_OPTION) {
            try {
                ImageIO.write(bufferPanel, "png", selectorFicheros.getSelectedFile());
            } catch (IOException ex) {
                // Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Método para mostrar el <code>JDialog</code> donde esta el
     * <code>JFileChooser</code> usado para navegar entre los archivos de la
     * máquina.
     *
     * @param vista <code>JDialog</code> que contiene el
     * <code>JFileChooser</code>.
     */
    public void mostrarVista(JDialog vista) {
        vista.setVisible(true);
    }

    /**
     * Método para esconder el <code>JDialog</code> donde esta el
     * <code>JFileChooser</code> usado para navegar entre los archivos de la
     * máquina.
     *
     * @param vista <code>JDialog</code> que contiene el
     * <code>JFileChooser</code>.
     */
    public void esconderVista(JDialog vista) {
        vista.setVisible(false);
    }
}
