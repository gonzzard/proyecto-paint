package utilidades;

import java.awt.BasicStroke;
import java.awt.Stroke;
import javax.swing.JSlider;

/**
 *
 * @author Gonzalo de las Heras
 */
public class GestorTrazos {

    /**
     * Objeto de tipo <code>JSlider</code> que indica el ancho del trazo.
     */
    private javax.swing.JSlider anchoTrazo;
    /**
     * Variable de tipo <code>float</code> que guarda la separación de la línea
     * discontinua.
     */
    private final float dash[];
    /**
     * Objetode tipo <code>Stroke</code> que guarda el trazo discontinuo.
     */
    private final Stroke dis;
    /**
     * Objeto de tipo <code>Stroke</code> que almacena las características del
     * ancho del trazo seleccionado para dibujar.
     */
    private Stroke anchoTrazoSelec;

    /**
     * Método que devuelve el trazo discontinuo.
     *
     * @return Devuelve el trazo discontinuo.
     */
    public Stroke getDis() {
        return dis;
    }

    /**
     * Método que devuelve el objeto <code>JSlider</code> que indica el ancho
     * del trazo.
     *
     * @return Devuelve el objeto <code>JSlider</code> que indica el ancho del
     * trazo.
     */
    public JSlider getAnchoTrazo() {
        return anchoTrazo;
    }

    /**
     * Método que actualiza el objeto <code>JSlider</code> que indica el ancho
     * del trazo.
     *
     * @param anchoTrazo objeto <code>JSlider</code> que indica el ancho del
     * trazo.
     */
    public void setAnchoTrazo(JSlider anchoTrazo) {
        this.anchoTrazo = anchoTrazo;
    }

    /**
     * Método que devuelve el ancho del trazo que ha sido seleccionado.
     *
     * @return Devuelve el ancho del trazo que ha sido seleccionado.
     */
    public Stroke getAnchoTrazoSelec() {
        return anchoTrazoSelec;
    }

    /**
     * Método que actualiza el objeto <code>Stroke</code> el trazo seleccionado.
     *
     * @param anchoTrazoSelec objeto <code>Stroke</code> el trazo seleccionado.
     */
    public void setAnchoTrazoSelec(Stroke anchoTrazoSelec) {
        this.anchoTrazoSelec = anchoTrazoSelec;
    }

    /**
     * Constructor de la clase.
     *
     * @param anchoTrazo <code>JSlider</code> que indica el ancho del trazo.
     */
    public GestorTrazos(javax.swing.JSlider anchoTrazo) {
        this.dash = new float[]{10.f};
        this.dis = new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        this.anchoTrazoSelec = new BasicStroke(anchoTrazo.getValue());
        this.anchoTrazo = anchoTrazo;
    }
}
