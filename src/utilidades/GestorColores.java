package utilidades;

import java.awt.Color;
import javax.swing.JDialog;

/**
 *
 * @author Gonzalo de las Heras
 */
public class GestorColores {

    /**
     * Objeto de tipo <code>Color</code> que se empleará para almacenar el color
     * seleccionado para el trazo.
     */
    private Color colorTrazoSelec;
    /**
     * Objeto de tipo <code>Color</code> que se empleará para almacenar el color
     * seleccionado para el relleno de las figuras.
     */
    private Color colorRellenoSelec;

    /**
     * Constructor de la clase.
     */
    public GestorColores() {
        this.colorRellenoSelec = null;
        this.colorTrazoSelec = new Color(Color.BLACK.getRGB());
    }

    /**
     * Método para mostrar el <code>JDialog</code> donde esta el
     * <code>JColorChooser</code> usado para seleecionar el color deseado.
     *
     * @param vista <code>JDialog</code> que contiene el
     * <code>JColorChooser</code>.
     */
    public void mostrarVista(JDialog vista) {
        vista.setVisible(true);
    }

    /**
     * Método para esconder el <code>JDialog</code> donde esta el
     * <code>JColorChooser</code> usado para seleecionar el color deseado.
     *
     * @param vista <code>JDialog</code> que contiene el
     * <code>JColorChooser</code>.
     */
    public void esconderVista(JDialog vista) {
        vista.setVisible(false);
    }

    /**
     * Método que devuelve el color del trazo que ha sido seleccionado.
     *
     * @return Devuelve el color del trazo que ha sido seleccionado.
     */
    public Color getColorTrazoSelec() {
        return colorTrazoSelec;
    }

    /**
     * Método que actualiza el color del trazo que ha sido seleccionado.
     *
     * @param colorTrazoSelec Color del trazo que ha sido seleccionado.
     */
    public void setColorTrazoSelec(Color colorTrazoSelec) {
        this.colorTrazoSelec = colorTrazoSelec;
    }

    /**
     * Método que devuelve el color de relleno que ha sido seleccionado.
     *
     * @return Devuelve el color de relleno que ha sido seleccionado.
     */
    public Color getColorRellenoSelec() {
        return colorRellenoSelec;
    }

    /**
     * Método que actualiza el color del relleno que ha sido seleccionado.
     *
     * @param colorRellenoSelec Color del relleno que ha sido seleccionado.
     */
    public void setColorRellenoSelec(Color colorRellenoSelec) {
        this.colorRellenoSelec = colorRellenoSelec;
    }

}
