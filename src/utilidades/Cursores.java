package utilidades;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author Gonzalo de las Heras
 */
public class Cursores {

    /**
     * Objeto de tipo <code>img</code> empleado para capturar la imagen de cada
     * icono.
     */
    public Image img;
    /**
     * Objeto de tipo <code>Cursor</code> que almacena el cursor de la opción de
     * dibujo libre.
     */
    public Cursor cursor;
    /**
     * Objeto de tipo <code>ArrayList</code> que almacena todos los cursores.
     */
    public ArrayList<Cursor> cursores = new ArrayList();

    /**
     * Constructor de la clase.
     */
    public Cursores() {
        this.img = null;
        this.cursor = null;
        this.crearCursores();
    }

    /**
     * Método que crea los distintos cursores y los almacena.
     */
    public void crearCursores() {
        /* Cursor lápiz */
        Cursor lapiz;
        this.img = Toolkit.getDefaultToolkit().createImage("src/icons/lapiz.png");
        lapiz = Toolkit.getDefaultToolkit().createCustomCursor(this.img, new Point(3, 26), "img");

        /* Cursor bote de pintura */
        Cursor botePintura;
        this.img = Toolkit.getDefaultToolkit().createImage("src/icons/colorRelleno.png");
        botePintura = Toolkit.getDefaultToolkit().createCustomCursor(this.img, new Point(27, 21), "img");

        /* Añado al ArrayList */
        this.cursores.add(lapiz);
        this.cursores.add(botePintura);
    }

    /**
     * Método que cambia el cursor de una ventana.
     *
     * @param ventana ventana en la que se cambiará el cursor.
     * @param selecCursor cursor seleccionado.
     */
    public void setCursores(JFrame ventana, int selecCursor) {
        if (selecCursor == 1) {
            ventana.setCursor(this.cursores.get(0));
        }
        if (selecCursor == 0) {
            ventana.setCursor(Cursor.CROSSHAIR_CURSOR);
        }
        if (selecCursor == 102) {
            ventana.setCursor(this.cursores.get(1));
        }
    }
}
