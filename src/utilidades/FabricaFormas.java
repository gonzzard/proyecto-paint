package utilidades;

import formas.*;
import java.awt.Color;
import java.awt.Stroke;

/**
 *
 * @author Gonzalo de las heras
 */
public class FabricaFormas {

    /**
     * Variable de tipo <code>boolean</code> que indica si se va a dibujar un
     * nuevo elemento.
     */
    public boolean nuevoDibujo;

    /**
     * Constructor de la clase
     */
    public FabricaFormas() {
        this.nuevoDibujo = true;
    }

    /**
     * Método encargado de crear las distintas figuras.
     *
     * @param opcion Indica que figura se va a crear.
     * @param colorTrazoSelec Indica el color del trazo de la figura.
     * @param colorRellenoSelec Indica el color de relleno de la figura.
     * @param anchoTrazoSelec Indica el ancho del trazo de la figura.
     * @param texto Indica el texto, si la figura es un texto.
     * @param evt Indica el evt del ratón (posición x, posición y).
     * @param dobleBuffer Indica el objeto <code>DobleBuffer</code> donde se generarán las figuras.
     * @param elementosDibujados Indica el objeto <code>ObjetosPintados</code> donde se guardan las figuras.
     * @return Retorna la figura creada con las características especificadas.
     */
    public Herramienta creaForma(int opcion, Color colorTrazoSelec, Color colorRellenoSelec, Stroke anchoTrazoSelec, String texto,
            java.awt.event.MouseEvent evt, DobleBuffer dobleBuffer, ObjetosPintados elementosDibujados) {
        Herramienta herramientaActual = null;
        switch (opcion) {
            /* Círculo */
            case 1:
                herramientaActual = new Circulo(colorTrazoSelec, anchoTrazoSelec, colorRellenoSelec);
                break;
            /* Cuadrado */
            case 2:
                herramientaActual = new Cuadrado(colorTrazoSelec, anchoTrazoSelec, colorRellenoSelec);
                break;
            /* Línea */
            case 3:
                herramientaActual = new Linea(colorTrazoSelec, anchoTrazoSelec);
                break;
            /* Libre */
            case 4:
                herramientaActual = new DibujoLibre(colorTrazoSelec, anchoTrazoSelec);
                break;
            /* Elipse */
            case 5:
                herramientaActual = new Elipse(colorTrazoSelec, anchoTrazoSelec, colorRellenoSelec);
                break;
            /* Rectangulo */
            case 6:
                herramientaActual = new Rectangulo(colorTrazoSelec, anchoTrazoSelec, colorRellenoSelec);
                break;
            /* Texto */
            case 7:
                herramientaActual = new DibujarTexto(texto, colorTrazoSelec, anchoTrazoSelec);
                herramientaActual.pintar(dobleBuffer.getGraficaPanel());
                opcion = 0;
                break;
            /* Borrar */
            case 8:
                herramientaActual = new Borrar(anchoTrazoSelec);
                break;
            /* Relleno */
            case 9:
                if (colorRellenoSelec != null) {
                    herramientaActual = new Relleno(colorRellenoSelec, dobleBuffer.getBufferPanel(), evt);
                    elementosDibujados.aniadir(herramientaActual);
                    this.nuevoDibujo = true;
                    // sino no pinta hasta que mueves el cursor
                    dobleBuffer.getGraficaPanel().drawImage(dobleBuffer.getBufferPanel(), 0, 0, null);
                }
                break;
            /* Polígono libre */
            case 10:
                herramientaActual = new Poligono(colorTrazoSelec, anchoTrazoSelec);
                this.nuevoDibujo = true;
                break;
        }
        return herramientaActual;
    }
}
